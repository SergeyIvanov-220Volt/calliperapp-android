package com.volt.calipersapp.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.volt.calipersapp.network.HTTPRequest;
import com.volt.calipersapp.resources.ResourceManager;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

/**
 * Created by SIvanov on 20.03.2015.
 */
public class BitmapUtils {

    private static final String TAG = "BitmapUtils";

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private static Bitmap decodeBitmapFromResource(Resources res, int resId,  ResourceManager.Size size) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, size.width, size.height);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static Bitmap decodeScaledBitmapFromResource(Resources res, int resId,  ResourceManager.Size size) {
        Bitmap bitmap = decodeBitmapFromResource(res, resId, size);
        return  Bitmap.createScaledBitmap(bitmap, size.width, size.height, true);
    }

    private static Bitmap loadBitmap(String imageUrl) {
        Bitmap bitmap = null;
        try {
//            Log.d(TAG, "imageUrl: " + imageUrl);
            URL url = new URL(imageUrl);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setSSLSocketFactory(HTTPRequest.getSslContext().getSocketFactory());
            bitmap = BitmapFactory.decodeStream(urlConnection.getInputStream());
        } catch (Exception e) {
            Log.e(TAG, "Failed to load image", e);
        }
        return  bitmap;
    }

    public static Bitmap loadScaledBitmap(String imageUrl,  ResourceManager.Size size) {
        Bitmap bitmap = loadBitmap(imageUrl);
        double scaleFactor = (double)size.height / (double)bitmap.getHeight();
        int width = (int)(bitmap.getWidth() * scaleFactor);
        return  Bitmap.createScaledBitmap(bitmap, width, size.height, true);
    }
}
