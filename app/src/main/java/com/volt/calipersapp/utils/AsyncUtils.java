package com.volt.calipersapp.utils;

import android.os.AsyncTask;

/**
 * Created by SIvanov on 01.04.2015.
 */
public class AsyncUtils {

    public interface ITask {
        public void perform(String param);
        public void onResult();
    }

    public static class Task extends AsyncTask<String, Void, Void> {

        private ITask task;

        public Task(ITask task) {
            this.task = task;
        }

        @Override
        protected Void doInBackground(String... params) {
            task.perform(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            task.onResult();
        }
    }

}
