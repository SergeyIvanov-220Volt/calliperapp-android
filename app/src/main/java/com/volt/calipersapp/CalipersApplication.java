package com.volt.calipersapp;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;


/**
 * Created by SIvanov on 18.03.2015.
 */
public class CalipersApplication extends Application {

    private static final int SESSION_TIMEOUT = 300;

    // google analytics
    // TODO: create project in google analytics
    private static final String TRACKING_ID = "";

    private Tracker gaTracker;

    synchronized public Tracker getTracker() {
        if (gaTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            gaTracker = analytics.newTracker(TRACKING_ID);
            gaTracker.setSessionTimeout(SESSION_TIMEOUT);
        }
        return gaTracker;
    }
}
