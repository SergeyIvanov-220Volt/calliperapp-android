package com.volt.calipersapp;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.volt.calipersapp.analytics.Analytics;
import com.volt.calipersapp.resources.ResourceManager;

/**
 * Created by SIvanov on 16.03.2015.
 */
public class SplashFragment extends Fragment {

    private Handler splashHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LinearLayout layout = (LinearLayout)inflater.inflate(R.layout.fragment_splash, container, false);

        ImageView splashImageVeiw = (ImageView)layout.findViewById(R.id.image);
        ResourceManager.getInstance().setSplash(getActivity(), splashImageVeiw);

        Analytics.sendScreenOpened(getActivity(), "Splash");
        return layout;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        splashHandler = new Handler();
        splashHandler.postDelayed(startNextFragment, 2000);
    }

    private Runnable startNextFragment = new Runnable() {
        @Override
        public void run() {
            ((CalipersActivity) getActivity()).setContentFragment(CalipersActivity.FRAGMENT_LEVEL);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        splashHandler.removeCallbacks(startNextFragment);
        splashHandler.removeCallbacks(startNextFragment);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((ActionBarActivity)getActivity()).getSupportActionBar().hide();
    }
}
