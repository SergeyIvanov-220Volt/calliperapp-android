package com.volt.calipersapp.analytics;

import android.app.Activity;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.volt.calipersapp.CalipersApplication;

/**
 * Created by SIvanov on 18.03.2015.
 */
public class Analytics {

    public static final String CATEGORY = "Events";

    public class Event {
        public static final String APP_OPENED = "CalipersAppOpened";
        public static final String GO_TO_FOREGROUND = "CalipersAppGoToForeground";
        public static final String TOGGLE_CAMERA_VIEW = "CalipersAppToggleCameraView";
        public static final String CLICK_ON_BANNER = "CalipersAppBannerClick";
        public static final String NO_BANNER = "CalipersAppNoBanner";
    }


    public static void sendScreenOpened(Activity activity, String screenName) {
        Tracker t = ((CalipersApplication)activity.getApplication()).getTracker();
        t.setScreenName(screenName);
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    public static void sendEvent(Activity activity, String type) {
        Tracker t = ((CalipersApplication)activity.getApplication()).getTracker();
        t.send(new HitBuilders.EventBuilder()
                .setCategory(Analytics.CATEGORY)
                .setAction(type)
                .build());
    }

    public static void sendEvent(Activity activity, String type, String label, long value) {
        Tracker t = ((CalipersApplication)activity.getApplication()).getTracker();
        t.send(new HitBuilders.EventBuilder()
                .setCategory(Analytics.CATEGORY)
                .setAction(type)
                .setLabel(label)
                .setValue(value)
                .build());
    }
}
