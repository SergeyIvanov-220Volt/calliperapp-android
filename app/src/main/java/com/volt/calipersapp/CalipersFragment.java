package com.volt.calipersapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.volt.calipersapp.analytics.Analytics;
import com.volt.calipersapp.resources.ResourceManager;
import com.volt.calipersapp.view.CameraPreview;
import org.codechimp.apprater.AppRater;

/**
 * Created by SIvanov on 20.02.2015.
 */
public class CalipersFragment extends Fragment {

    private static final String TAG = CalipersFragment.class.getName();

    private static final int DAYS_UNTIL_PROMPT = 7;
    private static final int LAUNCHES_UNTIL_PROMPT = 10;

    private long updateTime;

    private View contentView;
    private TextView currentLengthText;

    private float[] lastAccelerometerData = new float[3];

    private Camera camera;
    private CameraPreview preview;

    private float[] sensorDataSmooth = new float[2];

    private double userAngle;

    public int getUserAngle() {
        if(userAngle > 180) {
            userAngle -= 360;
        }
        return (int)userAngle;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        AppRater.app_launched(getActivity(), DAYS_UNTIL_PROMPT, LAUNCHES_UNTIL_PROMPT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.fragment_calipers, null);

        currentLengthText = (TextView)contentView.findViewById(R.id.current_length);

        String countryCode = Preferences.getString(getActivity(), Preferences.COUNTRY_CODE, Preferences.DEFAULT_COUNTRY_CODE);
        if(countryCode.equals(Preferences.DEFAULT_COUNTRY_CODE)) {
            ImageView bannerImageView = (ImageView) contentView.findViewById(R.id.banner);
            ResourceManager.getInstance().setBanner(getActivity(), bannerImageView);
            bannerImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Analytics.sendEvent(getActivity(), Analytics.Event.CLICK_ON_BANNER);
                    String url = ResourceManager.getUrlWithRef(getActivity());
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
            });
        }
        Analytics.sendScreenOpened(getActivity(), "Calipers");
        return contentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setCustomActionBar();
        boolean cameraEnabled = Preferences.getBoolean(getActivity(), Preferences.CAMERA_ENABLED, false);
        setCameraBackgroundEnabled(cameraEnabled, cameraEnabled);
    }

    private void setCustomActionBar() {
        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setCustomView(R.layout.action_bar_layout);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        boolean cameraEnabled = Preferences.getBoolean(getActivity(), Preferences.CAMERA_ENABLED, false);
        setCameraBackgroundEnabled(false, cameraEnabled);
    }

    private void enableCameraBackground () {
        if(camera != null) {
            return;
        }
        camera = getCamera();
        if (camera != null) {
            if(preview != null) {
                ((FrameLayout) contentView.findViewById(R.id.camera_view)).removeView(preview);
            }
            preview = new CameraPreview(getActivity(), camera);
            ((FrameLayout) contentView.findViewById(R.id.camera_view)).addView(preview);
        }
    }

    private void disableCameraBackground() {
        if(camera == null) {
            return;
        }
        camera.stopPreview();
        camera.release();
        if(preview != null) {
            ((FrameLayout) contentView.findViewById(R.id.camera_view)).removeView(preview);
            preview = null;
        }
        camera = null;
    }

    private boolean checkCamaraHardware(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    private Camera getCamera() {
        Camera cam = null;
        try {
            cam = Camera.open();
            cam.setDisplayOrientation(90);
        }
        catch (Exception e) {
            Log.e(TAG, "Cannot access camera", e);
        }
        return cam;
    }

    public void setCameraBackgroundEnabled(boolean enabled, boolean shadow) {
        if(!checkCamaraHardware(getActivity())) {
            setShadowEnabled(false);
            return;
        }
        if(enabled) {
            enableCameraBackground();
        } else {
            disableCameraBackground();
        }
        setShadowEnabled(shadow);
    }

    public void setShadowEnabled(boolean enabled) {

    }
}
