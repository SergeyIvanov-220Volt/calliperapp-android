package com.volt.calipersapp;

import android.content.Context;

/**
 * Created by SIvanov on 20.03.2015.
 */
public class Preferences {

    private static final String SHARED_PREFERENCES_NAME = "level_preferences";
    public static final String NEED_DISPLAY_TUTORIAL = "need_display_turorial";
    public static final String CAMERA_ENABLED = "camera_enabled";
    public static final String DOWNLOAD_TIME = "download_time";
    public static final String BANNERS_DATA = "banners_data";
    public static final String COUNTRY_CODE = "country_code";

    public static final String URL_BANNERS = "banners";
    public static final String DEFAULT_COUNTRY_CODE = "RU";

    public static boolean getBoolean(Context context, String preferenceName, boolean defaultValue) {
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).getBoolean(preferenceName, defaultValue);
    }

    public static long getLong(Context context, String preferenceName, long defaultValue) {
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).getLong(preferenceName, defaultValue);
    }

    public static String getString(Context context, String preferenceName, String defaultValue) {
        return context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).getString(preferenceName, defaultValue);
    }

    public static void setBoolean(Context context, String preferenceName, boolean value) {
        context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(preferenceName, value)
                .commit();
    }

    public static void setLong(Context context, String preferenceName, long value) {
        context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
                .edit()
                .putLong(preferenceName, value)
                .commit();
    }

    public static void setString(Context context, String preferenceName, String value) {
        context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
                .edit()
                .putString(preferenceName, value)
                .commit();
    }
}
