package com.volt.calipersapp.service;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.volt.calipersapp.Preferences;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by SIvanov on 25.05.2015.
 */
public class AddressIntentService extends IntentService {

    protected ResultReceiver receiver;
    private static final String TAG = "AddressIntentService";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public AddressIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        Location location = intent.getParcelableExtra(Constants.LOCATION_DATA_EXTRA);
        receiver = intent.getParcelableExtra(Constants.RECEIVER);
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException ioException) {
            Log.e(TAG, "Service not available", ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            Log.e(TAG, "Invalid parameters: " + "Latitude = " + location.getLatitude() + ", Longitude = " + location.getLongitude(), illegalArgumentException);
        }

        if (addresses == null || addresses.size()  == 0) {
            //Log.d(TAG, "No address found");
            deliverResultToReceiver(Preferences.DEFAULT_COUNTRY_CODE);
        } else {
            String countryCode = addresses.get(0).getCountryCode();
            deliverResultToReceiver(countryCode);
        }
    }

    private void deliverResultToReceiver(String result) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.RESULT_DATA_KEY, result);
        receiver.send(Constants.SUCCESS_RESULT, bundle);
    }
}
