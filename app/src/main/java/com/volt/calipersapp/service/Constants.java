package com.volt.calipersapp.service;

/**
 * Created by SIvanov on 25.05.2015.
 */
public class Constants {
    public static final int SUCCESS_RESULT = 0;
    public static final String PACKAGE_NAME = "com.volt.levelapp.service";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
}
