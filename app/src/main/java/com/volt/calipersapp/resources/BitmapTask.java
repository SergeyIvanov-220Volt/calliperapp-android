package com.volt.calipersapp.resources;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.volt.calipersapp.Preferences;
import com.volt.calipersapp.R;
import com.volt.calipersapp.network.DownloadManager;
import com.volt.calipersapp.network.RequestManager;

import org.json.JSONArray;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.Random;

/**
 * Created by SIvanov on 31.03.2015.
 */
class BitmapTask extends AsyncTask<Integer, Void, Bitmap> {

    private static final String TAG = "BitmapTask";
    private final WeakReference<ImageView> imageViewReference;
    private final WeakReference<Context> contextWeakReference;
    private int type;

    public BitmapTask(Context context, ImageView imageView) {
        imageViewReference = new WeakReference<ImageView>(imageView);
        contextWeakReference = new WeakReference<Context>(context);
    }

    @Override
    protected Bitmap doInBackground(Integer... params) {
        type = params[0];
        Bitmap bitmap = null;
        switch (type) {
            case ResourceManager.TYPE_SPLASH:
                bitmap = getSplash(contextWeakReference.get());
                break;
            case ResourceManager.TYPE_BANNER:
                bitmap = getBanner(contextWeakReference.get());
                break;
        }
        return bitmap;
    }

    private Bitmap getBanner(Context context) {
        Bitmap bitmap = null;
        try {
            JSONArray bannersArray = new JSONArray(Preferences.getString(context, Preferences.BANNERS_DATA, "[]"));
            if(bannersArray.length() > 0) {
                int bannerIndex = new Random().nextInt(bannersArray.length());
                String fileName = bannersArray.getJSONObject(bannerIndex).getString(RequestManager.KEY_FILE_NAME);
                String url = bannersArray.getJSONObject(bannerIndex).getString(RequestManager.KEY_URL);
                bitmap = BitmapFactory.decodeFile(fileName);
                ResourceManager.bannerUrl = url;
            }
        } catch (Exception e) {
            Log.e(TAG, "Can not get banner", e);
        }
        return bitmap;
    }

    private Bitmap getSplash(Context context) {
        Bitmap splash = null;
        try {
            String dir = DownloadManager.getFileDir(context, DownloadManager.SPLASH_DIR);
            File[]splashes = (new File(dir)).listFiles();
            if(splashes.length > 0) {
                splash = BitmapFactory.decodeFile(splashes[splashes.length - 1].getPath());
            }

        } catch (Exception e) {
            Log.e(TAG, "Can not get banner", e);
        }
        return splash;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (imageViewReference != null) {
            final ImageView imageView = imageViewReference.get();
            if( bitmap != null) {
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            } else {
                switch (type) {
                    case ResourceManager.TYPE_SPLASH:
                        imageView.setImageResource(R.drawable.splash);
                        break;
                    case ResourceManager.TYPE_BANNER:
                        boolean firstBanner =  (new Random().nextInt() % 2 == 0);
                        imageView.setImageResource(firstBanner ? R.drawable.banner_1 : R.drawable.banner_2);
                        ResourceManager.bannerUrl = ResourceManager.getInstance().getResources().getString(firstBanner ?  R.string.banner_default_url_1 : R.string.banner_default_url_2);
                        break;
                }
            }
        }
    }
}
