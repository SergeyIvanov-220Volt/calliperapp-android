package com.volt.calipersapp.resources;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.ImageView;
import com.volt.calipersapp.R;
import com.volt.calipersapp.utils.BitmapUtils;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by SIvanov on 24.03.2015.
 */
public class ResourceManager {

    private static final String TAG = "ResourceManager";

    public static final int TYPE_SPLASH = 1;
    public static final int TYPE_BANNER = 2;

    private static ResourceManager instance;
    public static String bannerUrl;
    private int actionBarHeight;

    public static ResourceManager getInstance() {
        if(instance == null) {
            throw new NullPointerException("ResourceManager is not initialized");
        }
        return instance;
    }

    public static void initResourceManager(Activity activity) {
        instance = new ResourceManager(activity);
    }

    private static Map<Integer, Bitmap> bitmaps = new HashMap<Integer, Bitmap>();

    public class Size {
        final public int width;
        final public int height;

        public Size(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }

    private Size screenSize;
    private Size scaleSize;

    private Resources resources;

    public Resources getResources() {
        return resources;
    }

    public ResourceManager(Activity activity) {
        this.resources = activity.getResources();
        initializeActionBarHeight(activity);
    }

    public void initializeActionBarHeight(Activity activity) {
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv,true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        this.actionBarHeight = actionBarHeight;
    }

    private Size getSize(int resId) {
        Size size = null;
        switch (resId) {
            case R.drawable.up:
            case R.drawable.down:
                size = getLabelSize();
                break;
            default:
                throw new IllegalArgumentException("Resource size is not set");
        }
        return size;
    }

    public Size getScreenSize() {
        if(screenSize == null) {
            DisplayMetrics dm = resources.getDisplayMetrics();
            int width = (dm.widthPixels < dm.heightPixels) ? dm.widthPixels : dm.heightPixels;
            int height = (dm.widthPixels > dm.heightPixels) ? dm.widthPixels : dm.heightPixels;
            screenSize = new Size(width, height);
        }
        return screenSize;
    }

    public Size getLabelSize() {
        int width = (int)(getScreenSize().width * 0.2);
        int height = (int)(getScreenSize().width * 0.2);
        return new Size(width, height);
    }

    public Size getBannerSize() {
        int width = screenSize.width;
        int height = actionBarHeight;
        return new Size(width, height);
    }

    public void setResource(ImageView iv, int resId) {
        iv.setImageBitmap(getBitmap(resId));
    }

    public Bitmap getBitmap(int resId) {
        if(bitmaps.get(resId) == null) {
            bitmaps.put(resId, BitmapUtils.decodeScaledBitmapFromResource(resources, resId, getSize(resId)));
        }
        return bitmaps.get(resId);
    }

    public void setSplash(Context context, ImageView imageView) {
        (new BitmapTask(context, imageView)).execute(TYPE_SPLASH);
    }

    public void setBanner(Context context, ImageView imageView) {
        (new BitmapTask(context, imageView)).execute(TYPE_BANNER);
    }

    public Bitmap loadBitmapFromUrl(String url, int type) {
        Size size = null;
        switch (type) {
            case TYPE_SPLASH:
                size = getScreenSize();
                break;
            case TYPE_BANNER:
                size = getBannerSize();
                break;
            default:
                throw new IllegalArgumentException("Unsupported loaded image type");
        }
        return BitmapUtils.loadScaledBitmap(url, size);
    }

    public static String getUrlWithRef(Context context) {
        String url = (bannerUrl != null ? bannerUrl : context.getString(R.string.banner_default_url_1));
        url += (url.contains("?") ? "&" : "?") +  context.getString(R.string.ref_url_param);
        return url;
    }
}
