package com.volt.calipersapp;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.os.PersistableBundle;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.volt.calipersapp.analytics.Analytics;
import com.volt.calipersapp.resources.ResourceManager;
import com.volt.calipersapp.service.AddressIntentService;
import com.volt.calipersapp.service.Constants;

public class CalipersActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final int FRAGMENT_SPLASH = 0;
    public static final int FRAGMENT_LEVEL = 1;

    private Menu menu;
    private GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ResourceManager.initResourceManager(this);
        setContentView(R.layout.activity_calipers);
        if (savedInstanceState == null) {
            setContentFragment(FRAGMENT_SPLASH);
        }
        if (Preferences.getString(this, Preferences.COUNTRY_CODE, "").isEmpty()) {
            buildGoogleApiClient();
        }
    }

    public void setContentFragment(int fragmentId) {
        switch (fragmentId) {
            case FRAGMENT_SPLASH:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new SplashFragment())
                        .commit();
                break;
            case FRAGMENT_LEVEL:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new CalipersFragment())
                        .commit();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        YandexMetrica.onResumeActivity(this);
//        Adjust.onResume();
        Analytics.sendEvent(this, Analytics.Event.APP_OPENED);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        YandexMetrica.onPauseActivity(this);
//        Adjust.onResume();
        Analytics.sendEvent(this, Analytics.Event.GO_TO_FOREGROUND);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        FlurryAgent.onStartSession(this);
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        //        FlurryAgent.onEndSession(this);
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            getMenuInflater().inflate(R.menu.menu_calipers, menu);
            return true;
        }
        return false;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean enabled = Preferences.getBoolean(this, Preferences.CAMERA_ENABLED, false);
        menu.findItem(R.id.action_camera_off).setVisible(enabled);
        menu.findItem(R.id.action_camera_on).setVisible(!enabled);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        switch (item.getItemId()) {
            case R.id.action_camera_on:
                if(fragment instanceof CalipersFragment) {
                    Analytics.sendEvent(this, Analytics.Event.TOGGLE_CAMERA_VIEW);
                    Preferences.setBoolean(this, Preferences.CAMERA_ENABLED, true);
                    ((CalipersFragment)fragment).setCameraBackgroundEnabled(true, true);
                    invalidateOptionsMenu();
                }
                return true;
            case R.id.action_camera_off:
                if(fragment instanceof CalipersFragment) {
                    Analytics.sendEvent(this, Analytics.Event.TOGGLE_CAMERA_VIEW);
                    Preferences.setBoolean(this, Preferences.CAMERA_ENABLED, false);
                    ((CalipersFragment)fragment).setCameraBackgroundEnabled(false, false);
                    invalidateOptionsMenu();                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            // Determine whether a Geocoder is available.
            if (!Geocoder.isPresent()) {
                //Log.d(TAG, "No geocoder implemented");
                Preferences.getString(this, Preferences.COUNTRY_CODE, Preferences.DEFAULT_COUNTRY_CODE);
                return;
            }

            if (mGoogleApiClient.isConnected() && mLastLocation != null) {
                startIntentService();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Log.d(TAG, "GoogleApi connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Log.d(TAG, "GoogleApi connection failed");
    }

    protected void startIntentService() {
        Intent intent = new Intent(this, AddressIntentService.class);
        intent.putExtra(Constants.RECEIVER, new AddressResultReceiver(new Handler()));
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mLastLocation);
        startService(intent);
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            Preferences.setString(CalipersActivity.this, Preferences.COUNTRY_CODE, resultData.getString(Constants.RESULT_DATA_KEY));
        }
    }
}
