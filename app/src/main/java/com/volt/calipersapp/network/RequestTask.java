package com.volt.calipersapp.network;

import android.os.AsyncTask;

/**
 * Created by SIvanov on 30.03.2015.
 */
public class RequestTask extends AsyncTask<Void, String, String> {

    public interface IRequestListener {
        public void onRequestResult(String result);
    }

    private String url;
    private String data;
    private IRequestListener listener;

    public RequestTask(String url, String data, IRequestListener listener) {
        this.url = url;
        this.data = data;
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {
        return HTTPRequest.sendPostRequest(url, data);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        listener.onRequestResult(result);
    }
}
