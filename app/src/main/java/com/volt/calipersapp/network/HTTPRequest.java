package com.volt.calipersapp.network;

import android.util.Log;

import com.volt.calipersapp.R;
import com.volt.calipersapp.resources.ResourceManager;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by SIvanov on 30.03.2015.
 */
public class HTTPRequest {

    private static final String TAG = "HTTPRequest";

    public static String sendPostRequest(String url, String jsonData) {
        String result = "";
        HttpResponse response = null;
        try {
            HttpClient client = getHttpClient();
//            Log.d(TAG, "request: " + url);
//            Log.d(TAG, "json: " + jsonData);
            HttpPost request = new HttpPost(new URI(url));
            StringEntity entity = new StringEntity(jsonData);
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            request.setEntity(entity);
            response = client.execute(request);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            result = reader.readLine();
//            Log.d(TAG, "result: " + result);
        } catch (Exception e) {
            Log.e(TAG, "sendGetRequest failed", e);
        }
        return result;
    }

    public static HttpClient getHttpClient() throws IOException, NoSuchAlgorithmException, KeyManagementException, URISyntaxException, KeyStoreException, UnrecoverableKeyException {
        HttpClient client = new DefaultHttpClient();
        SSLSocketFactory ssf = getSslSocketFactory();
        ClientConnectionManager ccm = client.getConnectionManager();
        SchemeRegistry sr = ccm.getSchemeRegistry();
        sr.register(new Scheme("https", ssf, 443));
        DefaultHttpClient sslClient = new DefaultHttpClient(ccm, client.getParams());
        return sslClient;
    }

    private static KeyStore getKeyStore() {
        KeyStore trusted  = null;
        try {
            trusted = KeyStore.getInstance("BKS");
            InputStream in = ResourceManager.getInstance().getResources().openRawResource(R.raw.voltus);
            trusted.load(in, "android".toCharArray());
        } catch (Exception e) {
            Log.e(TAG, "getKeyStore failed", e);
        }
        return trusted;
    }

    private static SSLSocketFactory getSslSocketFactory() {
        SSLSocketFactory sf = null;
        try {
            sf = new SSLSocketFactory(getKeyStore());
            sf.setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
        } catch (Exception e) {
            Log.e(TAG, "getSslSocketFactory failed", e);
        }
        return sf;
    }


    public static SSLContext getSslContext() {
        SSLContext context = null;
        try {
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(getKeyStore());
            context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);
        } catch (Exception e) {
            Log.d(TAG, "getSslContext failed");
        }
        return context;
    }
}
