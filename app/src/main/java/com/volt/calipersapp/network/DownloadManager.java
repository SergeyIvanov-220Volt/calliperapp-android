package com.volt.calipersapp.network;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import com.volt.calipersapp.Preferences;
import com.volt.calipersapp.resources.ResourceManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by SIvanov on 30.03.2015.
 */
public class DownloadManager {

    private static final String TAG = DownloadManager.class.getName();

    private static final long UPDATE_PERIOD_INTERVAL = 24 * 60 * 60 * 1000;     // 1 day
    public static final String BANNERS_DIR = "banners";
    public static final String SPLASH_DIR = "splash";

    private Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public DownloadManager(Activity activity) {
        this.activity = activity;
    }

    public void performDownload(String baseUrl) {
        long lastDownloadTime = Preferences.getLong(activity, Preferences.DOWNLOAD_TIME, 0);
        if (System.currentTimeMillis() - lastDownloadTime < UPDATE_PERIOD_INTERVAL) {
            return;
        }
        if(!checkNetworkExists()) {
            return;
        }
        Preferences.setLong(activity, Preferences.DOWNLOAD_TIME, System.currentTimeMillis());
        (new RequestManager(this)).getImageData(baseUrl);
    }

    private boolean checkNetworkExists() {
        boolean status=false;
        try{
            ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                status = true;
            }
        }catch(Exception e){
            Log.e(TAG, "Failed to access network state", e);
        }
        return status;
    }

    public void clearPreviouslyLoadedImages() {
        File[] dirs = {new File(getFileDir(activity, SPLASH_DIR)), new File(getFileDir(activity, BANNERS_DIR))};
        for (int i=0; i<dirs.length; i++) {
            if(dirs[i] == null) {
                continue;
            }
            File[] files = dirs[i].listFiles();
            for(int j=0; j<files.length; j++) {
                if(!files[j].delete()) {
                    Log.e(TAG, "Could not delete file");
                }
            }
        }
    }

    public String saveFile(String url, int type) {
        String filePath = null;
        if(!isExternalStorageWritable()) {
            return filePath;
        }
        FileOutputStream out = null;
        try {
            filePath = getFilePath(activity, type);
            out = new FileOutputStream(filePath);
            Bitmap bmp = ResourceManager.getInstance().loadBitmapFromUrl(url, type);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (Exception e) {
            Log.e(TAG, "downloading file failed", e);
            filePath = null;
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    Log.e(TAG, "close file stream failed", e);
                }
            }
        }
        return filePath;
    }

    private static String getFilePath(Context context, int type) {
        String filePath = null;
        switch (type) {
            case ResourceManager.TYPE_BANNER:
                filePath = getFileDir(context, BANNERS_DIR) + "/banner_" + UUID.randomUUID().toString() + ".png";
                break;
            case ResourceManager.TYPE_SPLASH:
                filePath = getFileDir(context, SPLASH_DIR) + "/splash_" + UUID.randomUUID().toString() + ".png";
                break;
        }
        return filePath;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static String getFileDir(Context context, String dir) {
        File file = new File(context.getExternalCacheDir() + "/" + dir);
        if(!file.mkdirs()){
            Log.e(TAG, "Directory not created");
        }
        return file.getPath();
    }

}
