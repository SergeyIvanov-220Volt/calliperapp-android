package com.volt.calipersapp.network;

import android.util.Log;
import com.volt.calipersapp.Preferences;
import com.volt.calipersapp.analytics.Analytics;
import com.volt.calipersapp.resources.ResourceManager;
import com.volt.calipersapp.utils.AsyncUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by SIvanov on 30.03.2015.
 */
public class RequestManager implements RequestTask.IRequestListener {

    private static final String TAG = RequestManager.class.getName();

    public static final String KEY_DATA = "data";
    public static final String KEY_TYPE = "type";
    public static final String KEY_PARAMS = "params";
    public static final String KEY_WIDTH = "width";
    public static final String KEY_STATUS = "status";
    public static final String KEY_BANNERS = "banners";
    public static final String KEY_SPLASH = "splash";
    public static final String KEY_IMG = "img";
    public static final String KEY_URL = "url";
    public static final String KEY_FILE_NAME = "file_name";

    private static final String VALUE_BANNER = "banner";
    private static final String VALUE_SPLASH = "splash";
    private static final String VALUE_SUCCESS = "success";

    private DownloadManager manager;

    public RequestManager(DownloadManager manager) {
        this.manager = manager;
    }

    public void getImageData(String baseUrl) {
        try {
            String url = baseUrl + Preferences.URL_BANNERS;
            JSONObject object = new JSONObject()
                    .put(KEY_DATA, new JSONArray()
                        .put(getBannerObject())
                        .put(getSplashObject()));
            new RequestTask(url, object.toString(), this).execute();
        } catch (JSONException e) {
            Log.e(TAG, "Failed to get image data", e);
        }
    }

    private JSONObject getBannerObject() throws JSONException {
        return new JSONObject()
                .put(KEY_TYPE, VALUE_BANNER)
                .put(KEY_PARAMS, new JSONObject()
                        .put(KEY_WIDTH, ResourceManager.getInstance().getScreenSize().width));
    }

    private JSONObject getSplashObject() throws JSONException {
        return new JSONObject()
                .put(KEY_TYPE, VALUE_SPLASH)
                .put(KEY_PARAMS, new JSONObject()
                        .put(KEY_WIDTH, ResourceManager.getInstance().getScreenSize().width));
    }

    @Override
    public void onRequestResult(String result) {
        new AsyncUtils.Task(new AsyncUtils.ITask() {
            @Override
            public void perform(String param) {
                processResponse(param);
            }

            @Override
            public void onResult() {

            }
        }).execute(result);
    }

    public void processResponse(String result) {
        try {
            JSONObject object = new JSONObject(result);
            if(object.getString(KEY_STATUS).equals(VALUE_SUCCESS)) {
                manager.clearPreviouslyLoadedImages();

                JSONObject data = object.getJSONObject(KEY_DATA);
                if(data.has(KEY_SPLASH)) {
                    String url = data.getString(KEY_SPLASH);
                    manager.saveFile(url, ResourceManager.TYPE_SPLASH);
                }
                if(data.has(KEY_BANNERS)) {
                    JSONArray bannerData = new JSONArray();
                    JSONArray banners = data.getJSONArray(KEY_BANNERS);
                    for (int i=0; i<banners.length(); i++) {
                        JSONObject banner = banners.getJSONObject(i);
                        String url = banner.getString(KEY_IMG);
                        String bannerUrl = banner.getString(KEY_URL);
                        String filePath = manager.saveFile(url, ResourceManager.TYPE_BANNER);
                        if(filePath != null) {
                            bannerData.put(new JSONObject()
                                    .put(KEY_FILE_NAME, filePath)
                                    .put(KEY_URL, bannerUrl));
                        }
                    }
                    Preferences.setString(manager.getActivity(), Preferences.BANNERS_DATA, bannerData.toString());
                    if(banners.length() == 0) {
                        Analytics.sendEvent(manager.getActivity(), Analytics.Event.NO_BANNER, "width", ResourceManager.getInstance().getScreenSize().width);
                    }
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Failed to parse result", e);
        }
    }

}
